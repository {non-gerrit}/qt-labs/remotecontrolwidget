#include <QtGui/QApplication>
#include "remotecontrolwidget.h"
#include "scriptadapter.h"
#include "components/scriptui.h"
#include "components/locationui.h"
#include "components/batterybutton.h"
#include "components/powerbutton.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    RemoteControlWidget w;

    LocationUi locationUi;
    w.addToolBoxPage(&locationUi);

    BatteryButton batteryButton;
    w.addMenuButton(&batteryButton);

    PowerButton powerButton;
    w.addMenuButton(&powerButton);

    ScriptAdapter adapter;
    adapter.addScriptInterface("location", locationUi.scriptInterface());
    ScriptUi scriptUi(&adapter);
    w.addToolBoxPage(&scriptUi);

    w.show();
    return a.exec();
}

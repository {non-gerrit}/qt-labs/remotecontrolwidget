#-------------------------------------------------
#
# Project created by QtCreator 2010-02-25T15:28:57
#
#-------------------------------------------------

QT       += core gui script

TARGET = example
TEMPLATE = app

INCLUDEPATH += ../library
LIBS += -L../library -lremotecontrolwidget -Wl,-rpath=../library

SOURCES += main.cpp


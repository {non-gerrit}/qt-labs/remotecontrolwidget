#include "optionsitem.h"

#include <QtCore/QString>
#include <QtGui/QAbstractButton>
#include <QtGui/QBoxLayout>
#include <QtGui/QFormLayout>
#include <QtGui/QLabel>

OptionsItem::OptionsItem(const QString &name, QWidget *inputWidget, bool fullWidth, QObject *parent)
    : QObject(parent)
    , mLabel(0)
    , mInputWidget(inputWidget)
    , mAdvancedPage(0)
    , mHidden(false)
    , mAdvanced(false)
{
    if (fullWidth)
        mType = FullWidthType;
    else {
        mType = DefaultType;

        mInputLayout = new QHBoxLayout();
        mInputLayout->addStretch();
        mInputLayout->addWidget(mInputWidget);
    }
    if (name != 0 && !name.isEmpty())
        mLabel = new QLabel(name);
}

OptionsItem::~OptionsItem()
{}

bool OptionsItem::addTag(const QString &tag)
{
    if (!mTags.contains(tag.toLower())) {
        mTags.append(tag.toLower());
        return true;
    }
    return false;
}

QLabel *OptionsItem::label() const
{
    return mLabel;
}

QWidget *OptionsItem::inputWidget() const
{
    return mInputWidget;
}

QHBoxLayout *OptionsItem::inputLayout() const
{
    return mInputLayout;
}

bool OptionsItem::setAdvancedPage(OptionsItem *advancedPage)
{
    QAbstractButton *inputButton = qobject_cast<QAbstractButton *>(mInputWidget);
    if (!inputButton)
        return false;
    mAdvancedPage = advancedPage;
    mType = AdvancedType;
    mAdvancedPage->setTags(mTags);
    mAdvancedPage->setVisible(false, true);
    disconnect(inputButton, SIGNAL(clicked()), this, SLOT(buttonClicked()));
    connect(inputButton, SIGNAL(clicked()), SLOT(buttonClicked()));
    return true;
}

OptionsItem *OptionsItem::advancedPage() const
{
    return mAdvancedPage;
}

void OptionsItem::setVisible(bool visible, bool explicitly)
{
    if (mLabel)
        mLabel->setVisible(visible);
    mInputWidget->setVisible(visible);
    if (!visible && explicitly)
        mHidden = true;
    else
        mHidden = false;
}

bool OptionsItem::addTags(const QStringList &tags)
{
    bool ret = true;
    foreach(const QString &tag, tags)
        if (!addTag(tag))
            ret = false;
    return ret;
}

bool OptionsItem::removeTag(const QString &tag)
{
    return (mTags.removeAll(tag.toLower()) > 0);
}

bool OptionsItem::removeTags(const QStringList &tags)
{
    bool ret = true;
    foreach (const QString &tag, tags)
        if (!removeTag(tag))
            ret = false;
    return ret;
}

bool OptionsItem::setTags(const QStringList &tags)
{
    mTags.clear();
    return addTags(tags);
}   

bool OptionsItem::matchTag(const QString &tag) const
{
    if (mLabel && mLabel->text().toLower().contains(tag.toLower()))
        return true;
    foreach (const QString &tagInList, mTags)
        if (tagInList.contains(tag.toLower()))
            return true;
    return false;
}

OptionsItem::OptionsType OptionsItem::type() const
{
    return mType;
}

bool OptionsItem::isExplicitlyHidden() const
{
    return mHidden;
}

void OptionsItem::setAdvanced(bool advanced)
{
    mAdvanced = advanced;
}

bool OptionsItem::isAdvanced() const
{
    return mAdvanced;
}

void OptionsItem::buttonClicked()
{
    emit toggleAdvanced(this);
}

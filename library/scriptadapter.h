#ifndef SCRIPTADAPTER_H
#define SCRIPTADAPTER_H

#include "remotecontrolwidget_global.h"
#include "fibers/fiber.h"

#include <QtCore/QObject>
#include <QtCore/QTimer>
#include <QtCore/QStringList>
#include <QtCore/QHash>
#include <QtScript/QScriptEngine>

class ScriptFiber;
class QMessageBox;

class REMOTECONTROLWIDGETSHARED_EXPORT ScriptAdapter : public QObject
{
    Q_OBJECT
public:
    explicit ScriptAdapter(QObject *parent = 0);
    virtual ~ScriptAdapter();

    Q_INVOKABLE void yield(int ms);
    Q_INVOKABLE void messageBox(const QString &text);

    void runAutostartScripts();
    ScriptFiber *script(int index);

    void addScriptInterface(const QString &name, QObject *interface);

public slots:
    ScriptFiber *run(const QString &filePath);

signals:
    void scriptStart(ScriptFiber *script) const;
    void scriptStop(int index) const;

private slots:
    void continueScripts();

private:
    QHash<QString, QObject *> mScriptInterfaces;
    QList<ScriptFiber *> mActiveScripts;
    QTimer *mScriptRunner;

    QMessageBox *mMessageBox;

    friend class ScriptFiber;
};

class ScriptFiber : public QObject, public Fiber
{
    Q_OBJECT
public:
    ScriptFiber(const QString &name, const QString &scriptCode,
                 ScriptAdapter *adapter);

    QString name() const
    { return mName; }

    void requestTerminate();
    bool isTerminating() const
    { return mTerminate; }

    void togglePause();
    bool isPaused() const
    { return mPaused; }

    void beginBlocking();
    void endBlocking();

public slots:
    void suspend();

protected:
    virtual void run();

private:
    QString mName;
    QString mScriptCode;
    ScriptAdapter *mAdapter;

    bool mPaused;
    bool mTerminate;

    QScriptEngine mEngine;
    QTimer mStopTimer;
};

#endif // SCRIPTADAPTER_H

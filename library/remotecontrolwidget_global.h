#ifndef REMOTECONTROLWIDGET_GLOBAL_H
#define REMOTECONTROLWIDGET_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(REMOTECONTROLWIDGET_LIBRARY)
#  define REMOTECONTROLWIDGETSHARED_EXPORT Q_DECL_EXPORT
#else
#  define REMOTECONTROLWIDGETSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // REMOTECONTROLWIDGET_GLOBAL_H

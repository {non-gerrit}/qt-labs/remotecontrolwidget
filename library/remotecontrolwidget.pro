#-------------------------------------------------
#
# Project created by QtCreator 2010-02-25T14:32:24
#
#-------------------------------------------------

TARGET = remotecontrolwidget
TEMPLATE = lib

CONFIG += hide_symbols

DEFINES += REMOTECONTROLWIDGET_LIBRARY

SOURCES += \
    remotecontrolwidget.cpp \
    toolbox.cpp \
    optionsitem.cpp \
    scriptadapter.cpp \
    style/stylehelper.cpp \
    style/styledbar.cpp \
    style/styleanimator.cpp \
    style/manhattanstyle.cpp \
    style/filterlineedit.cpp \
    style/fancylineedit.cpp \
    components/locationui.cpp \
    components/powerbutton.cpp \
    components/batterybutton.cpp \
    components/scriptui.cpp

HEADERS += \
    remotecontrolwidget.h \
    remotecontrolwidget_global.h \
    toolbox.h \
    optionsitem.h \
    scriptadapter.h \
    style/styledbar.h \
    style/styleanimator.h \
    style/qtcassert.h \
    style/manhattanstyle.h \
    style/filterlineedit.h \
    style/fancylineedit.h \
    style/stylehelper.h \
    components/locationui.h \
    components/powerbutton.h \
    components/batterybutton.h \
    components/scriptui.h

RESOURCES += \
    style/style.qrc \
    components/component.qrc

include(fibers/fibers.pri)

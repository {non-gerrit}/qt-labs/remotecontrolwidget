INCLUDEPATH += src/ui/style
DEPENDPATH += src/ui/style
HEADERS += manhattanstyle.h \
        stylehelper.h \
        styleanimator.h \
        qtcassert.h \
        styledbar.h \
        fancylineedit.h \
        filterlineedit.h
SOURCES += manhattanstyle.cpp \
        stylehelper.cpp \
        styleanimator.cpp \
        styledbar.cpp \
        fancylineedit.cpp \
        filterlineedit.cpp
RESOURCES += style.qrc

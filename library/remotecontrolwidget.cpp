#include "remotecontrolwidget.h"
#include "toolbox.h"
#include "optionsitem.h"
#include "style/filterlineedit.h"
#include "style/manhattanstyle.h"
#include "style/styledbar.h"

#include <qmath.h>
#include <QtCore/QSettings>
#include <QtCore/QThread>
#include <QtCore/QDir>
#include <QtGui/QApplication>
#include <QtGui/QDesktopWidget>
#include <QtGui/QScrollBar>
#include <QtGui/QMessageBox>
#include <QtGui/QToolButton>
#include <QtGui/QCloseEvent>
#include <QtGui/QScrollArea>
#include <QtGui/QFrame>
#include <QtGui/QBoxLayout>
#include <QtGui/QLabel>

RemoteControlWidget::RemoteControlWidget(QWidget *parent)
    : QWidget(parent)
{
    QString baseName = style()->objectName();
#ifdef Q_WS_X11
    if (baseName == QLatin1String("windows")) {
        // Sometimes we get the standard windows 95 style as a fallback
        // e.g. if we are running on a KDE4 desktop
        QByteArray desktopEnvironment = qgetenv("DESKTOP_SESSION");
        if (desktopEnvironment == "kde")
            baseName = QLatin1String("plastique");
        else
            baseName = QLatin1String("cleanlooks");
    }
#endif
    mManhattanStyle = new ManhattanStyle(baseName);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->setMargin(0);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);

    StyledBar *menuBar = initializeMenuBar();
    mainLayout->addWidget(menuBar);

    QLabel *simulateLabel = new QLabel(tr("Simulate"));
    QMargins labelMargins = simulateLabel->contentsMargins();
    QFont labelFont = simulateLabel->font();
    labelFont.setBold(true);
    labelFont.setPointSize(11);
    simulateLabel->setFont(labelFont);
    simulateLabel->setStyle(mManhattanStyle);
    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->setContentsMargins(9, 0, labelMargins.right(), 0);
    hLayout->addWidget(simulateLabel);
    StyledBar *simulateBar = new StyledBar();
    simulateBar->setStyle(mManhattanStyle);
    simulateBar->setLayout(hLayout);
    mainLayout->addWidget(simulateBar);

    QScrollArea *simulateArea = new QScrollArea;
    simulateArea->setWidgetResizable(true);

    mToolBox = new ToolBox(false);
    mToolBox->setStyle(mManhattanStyle);
    simulateArea->setWidget(mToolBox);
    mainLayout->addWidget(simulateArea);

    // Some styles have extra up/down buttons in the scrollbar. We only support
    // the plain scrollbars and therefore force the style to be derived from cleanlooks for them.
    QStyle *forceScrollbarStyle = new ManhattanStyle("cleanlooks");
    simulateArea->setStyle(forceScrollbarStyle);
    simulateArea->verticalScrollBar()->setStyle(forceScrollbarStyle);
    simulateArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    simulateArea->verticalScrollBar()->setProperty("styledScrollBar", true);

    mViewButton = new QToolButton();
    mViewButton->setFont(labelFont);
    mViewButton->setText(tr("View"));
    mViewButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    mViewButton->setArrowType(Qt::RightArrow);
    mViewButton->setSizePolicy(QSizePolicy::Expanding, mViewButton->sizePolicy().verticalPolicy());
    mViewButton->setStyle(mManhattanStyle);
    hLayout = new QHBoxLayout();
    hLayout->setMargin(0);
    hLayout->addWidget(mViewButton);
    StyledBar *viewBar = new StyledBar();
    viewBar->setStyle(mManhattanStyle);
    viewBar->setLayout(hLayout);
    mainLayout->addWidget(viewBar);
    connect(mViewButton, SIGNAL(clicked()), SLOT(toggleViewPaneVisible()));

    mViewArea = new QFrame();
    initializeViewArea(mViewArea);
    mainLayout->addWidget(mViewArea);
}

RemoteControlWidget::~RemoteControlWidget()
{

}

void RemoteControlWidget::filtersChanged()
{
    mToolBox->filtersChanged(mFilterLineEdit->typedText());
}

void RemoteControlWidget::addToolBoxPage(ToolBoxPage *page)
{
    mToolBox->addPage(page);
}

void RemoteControlWidget::addMenuButton(QToolButton *button)
{
    button->setStyle(mManhattanStyle);
    button->setProperty("noArrow", true);
    mMenuLayout->addWidget(button);
}

void RemoteControlWidget::writeSettings(const QString &vendor, const QString &name) const
{
    QSettings settings(vendor, name);

    settings.beginGroup("ConfigurationWidget");
    settings.setValue("size", size());
    settings.setValue("pos", pos());
    settings.endGroup();
}

void RemoteControlWidget::readSettings(const QString &vendor, const QString &name)
{
    QSettings settings(vendor, name);

    settings.beginGroup("ConfigurationWidget");
    if (settings.contains("size"))
        resize(settings.value("size").toSize());
    if (settings.contains("pos"))
        move(settings.value("pos").toPoint());
    settings.endGroup();
}

void RemoteControlWidget::toggleViewPaneVisible()
{
    if (mViewArea->isHidden()) {
        mViewArea->show();
        mViewButton->setArrowType(Qt::RightArrow);
    } else {
        mViewArea->hide();
        mViewButton->setArrowType(Qt::DownArrow);
    }
}

StyledBar *RemoteControlWidget::initializeMenuBar()
{
    mMenuLayout = new QHBoxLayout;
    mMenuLayout->setSpacing(0);
    mMenuLayout->setMargin(0);

    QHBoxLayout *toplayout = new QHBoxLayout;
    toplayout->setSpacing(0);
    toplayout->setMargin(0);
    toplayout->addLayout(mMenuLayout);
    toplayout->addStretch();
    mFilterLineEdit = new FilterLineEdit();
    mFilterLineEdit->setStyle(mManhattanStyle);
    connect(mFilterLineEdit, SIGNAL(filterChanged(const QString &)), SLOT(filtersChanged()));
    toplayout->addWidget(mFilterLineEdit);

    StyledBar *bar = new StyledBar;
    bar->setLayout(toplayout);
    bar->setStyle(mManhattanStyle);
    return bar;
}

void RemoteControlWidget::initializeViewArea(QFrame *f)
{
    QVBoxLayout *layout = new QVBoxLayout;
    QLabel *test = new QLabel(f);
    test->setText("Test!");
    layout->addWidget(test);
    f->setLayout(layout);
}

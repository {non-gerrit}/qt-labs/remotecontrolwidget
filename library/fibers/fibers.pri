INCLUDEPATH += fibers
DEPENDPATH += fibers

HEADERS += fiber.h
SOURCES += fiber.cpp

contains(QMAKE_CXX,g++) {
    win32 {
        # will fail for 64 bit win!
        SOURCES += \
            switchstack_gcc_32_win.cpp \
            initializestack_32.cpp
    }

    mac {
        CONFIG(x86_64) {
            SOURCES += \
                switchstack_gcc_64_linux_mac.s \
                initializestack_64_linux_mac.cpp
        } else {
            SOURCES += \
                switchstack_gcc_32_linux_mac.s \
                initializestack_32.cpp
        }
    }
    !win32:!mac {
        contains(QMAKE_CFLAGS,-m64) {
            SOURCES += \
                switchstack_gcc_64_linux_mac.s \
                initializestack_64_linux_mac.cpp
        } else {
            SOURCES += \
                switchstack_gcc_32_linux_mac.s \
                initializestack_32.cpp
        }
    }
}
win32:contains(QMAKE_CXX,cl) {
    # will fail for 64 bit win!
    SOURCES += \
        switchstack_msvc_32.cpp \
        initializestack_32.cpp
}

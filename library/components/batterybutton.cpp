#include "batterybutton.h"

#include <QtCore/QSignalMapper>
#include <QtGui/QMenu>
#include <QtGui/QIcon>

BatteryButton::BatteryButton(QWidget *parent)
    : QToolButton(parent)
{
    setPopupMode(QToolButton::InstantPopup);
    setDisplayedBatteryLevel(BatteryNormal);

    QMenu *menu = new QMenu(this);
    QAction *action = 0;

    action = menu->addAction(QIcon(":/components/images/battery_100.png"), tr("Normal"));
    action->setProperty("batteryLevel", BatteryNormal);
    connect(action, SIGNAL(triggered()), SLOT(emitBatteryLevelChanged()));

    action = menu->addAction(QIcon(":/components/images/battery_40.png"), tr("Low"));
    action->setProperty("batteryLevel", BatteryLow);
    connect(action, SIGNAL(triggered()), SLOT(emitBatteryLevelChanged()));

    action = menu->addAction(QIcon(":/components/images/battery_20.png"), tr("Very low"));
    action->setProperty("batteryLevel", BatteryVeryLow);
    connect(action, SIGNAL(triggered()), SLOT(emitBatteryLevelChanged()));

    action = menu->addAction(QIcon(":/components/images/battery_0.png"), tr("Critical"));
    action->setProperty("batteryLevel", BatteryCritical);
    connect(action, SIGNAL(triggered()), SLOT(emitBatteryLevelChanged()));

    setMenu(menu);
}

BatteryButton::~BatteryButton()
{
}

void BatteryButton::emitBatteryLevelChanged()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (!action)
        return;
    BatteryLevel newLevel = static_cast<BatteryLevel>(action->property("batteryLevel").toInt());

    setDisplayedBatteryLevel(newLevel);
    emit batteryLevelChanged(newLevel);
}

void BatteryButton::setDisplayedBatteryLevel(BatteryLevel level)
{
    QIcon newIcon;
    if (level == BatteryNormal)
        newIcon = QIcon(":/components/images/battery_100.png");
    else if (level == BatteryLow)
        newIcon = QIcon(":/components/images/battery_40.png");
    else if (level == BatteryVeryLow)
        newIcon = QIcon(":/components/images/battery_20.png");
    else if (level == BatteryCritical)
        newIcon = QIcon(":/components/images/battery_0.png");
    setIcon(newIcon);
}

#include "powerbutton.h"

#include <QtGui/QMenu>
#include <QtGui/QIcon>

PowerButton::PowerButton(QWidget *parent)
    : QToolButton(parent)
{
    setPopupMode(QToolButton::InstantPopup);
    setDisplayedState(BatteryPower);

    QMenu *menu = new QMenu(this);
    QAction *action = 0;

    action = menu->addAction(QIcon(":/components/images/unknownpower.png"), tr("Unknown or error"));
    action->setProperty("powerState", UnknownPower);
    connect(action, SIGNAL(triggered()), SLOT(emitPowerStateChanged()));

    action = menu->addAction(QIcon(":/components/images/batterypower.png"), tr("Battery"));
    action->setProperty("powerState", BatteryPower);
    connect(action, SIGNAL(triggered()), SLOT(emitPowerStateChanged()));

    action = menu->addAction(QIcon(":/components/images/wall.png"), tr("Wall"));
    action->setProperty("powerState", WallPower);
    connect(action, SIGNAL(triggered()), SLOT(emitPowerStateChanged()));

    action = menu->addAction(QIcon(":/components/images/wall_charge.png"), tr("Wall and charging"));
    action->setProperty("powerState", WallPowerChargingBattery);
    connect(action, SIGNAL(triggered()), SLOT(emitPowerStateChanged()));

    setMenu(menu);
}

PowerButton::~PowerButton()
{
}

void PowerButton::setDisplayedState(PowerState state)
{
    QIcon newIcon;
    if (state == UnknownPower)
        newIcon = QIcon(":/components/images/unknownpower.png");
    else if (state == BatteryPower)
        newIcon = QIcon(":/components/images/batterypower.png");
    else if (state == WallPower)
        newIcon = QIcon(":/components/images/wall.png");
    else if (state == WallPowerChargingBattery)
        newIcon = QIcon(":/components/images/wall_charge.png");
    setIcon(newIcon);
}

void PowerButton::emitPowerStateChanged()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (!action)
        return;
    PowerState newState = static_cast<PowerState>(action->property("powerState").toInt());

    setDisplayedState(newState);
    emit powerStateChanged(newState);
}

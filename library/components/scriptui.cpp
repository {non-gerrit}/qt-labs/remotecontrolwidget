#include "scriptui.h"

#include "scriptadapter.h"
#include "optionsitem.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QSignalMapper>
#include <QtGui/QGridLayout>
#include <QtGui/QAbstractItemView>
#include <QtGui/QPushButton>
#include <QtGui/QHeaderView>
#include <QtGui/QTableWidget>
#include <QtGui/QTreeView>
#include <QtGui/QFileSystemModel>
#include <QtGui/QToolButton>
#include <QtGui/QMenu>

ScriptUi::ScriptUi(ScriptAdapter *adapter, QWidget *parent)
    : ToolBoxPage(parent)
    , mAdapter(adapter)
{
    QStringList tags;
    QList<OptionsItem *> options;
    tags << tr("scripting");

    QGridLayout *scriptingLayout = new QGridLayout();
    {
        mScriptList = new QTableWidget(0, 2);
        mScriptList->verticalHeader()->setHidden(true);
        QStringList headerLabels;
        headerLabels << tr("Active scripts") << tr("Status");
        mScriptList->setHorizontalHeaderLabels(headerLabels);
        mScriptList->horizontalHeader()->setStretchLastSection(false);
        mScriptList->horizontalHeader()->setResizeMode(0, QHeaderView::Stretch);
        mScriptList->horizontalHeader()->setResizeMode(1, QHeaderView::Fixed);
        mScriptList->setSelectionMode(QAbstractItemView::SingleSelection);
        mScriptList->setSelectionBehavior(QAbstractItemView::SelectRows);

        scriptingLayout->addWidget(mScriptList, 1, 0);

        QVBoxLayout *scriptListButtonLayout = new QVBoxLayout();
        {
            QPushButton *abortButton = new QPushButton(tr("Abort"));
            QPushButton *pauseButton = new QPushButton(tr("Pause"));
            connect(abortButton, SIGNAL(clicked()), this, SLOT(abortScript()));
            connect(pauseButton, SIGNAL(clicked()), this, SLOT(togglePauseScript()));

            scriptListButtonLayout->addWidget(pauseButton);
            scriptListButtonLayout->addWidget(abortButton);
            scriptListButtonLayout->addStretch();
        }
        scriptingLayout->addLayout(scriptListButtonLayout, 1, 1);

        mFileView = new QTreeView();
        mFileModel = new QFileSystemModel(mFileView);
        QDir scriptsDir(QCoreApplication::applicationDirPath());
        scriptsDir.cd(QLatin1String("scripts"));
        QStringList nameFilters;
        nameFilters << "*.js" << "*.qs";
        mFileModel->setNameFilters(nameFilters);
        mFileModel->setNameFilterDisables(false);
        mFileModel->setRootPath(scriptsDir.path());
        mFileView->setModel(mFileModel);
        mFileView->setColumnHidden(1, true);
        mFileView->setColumnHidden(2, true);
        mFileView->setColumnHidden(3, true);
        mFileView->setHeaderHidden(true);
        mFileView->setRootIndex(mFileModel->index(scriptsDir.path()));
        connect(mFileView, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(runSelectedScript()));

        scriptingLayout->addWidget(mFileView, 0, 0);

        QVBoxLayout *scriptRunnerButtonLayout = new QVBoxLayout();
        {
            QPushButton *runButton = new QPushButton(tr("Run"));
            connect(runButton, SIGNAL(clicked()), this, SLOT(runSelectedScript()));

            scriptRunnerButtonLayout->addWidget(runButton);
            scriptRunnerButtonLayout->addStretch();
        }
        scriptingLayout->addLayout(scriptRunnerButtonLayout, 0, 1);
    }

    QWidget *tmp = new QWidget();
    tmp->setLayout(scriptingLayout);

    OptionsItem *item = new OptionsItem("", tmp, true);
    item->setTags(tags);
    options << item;

    connect(mAdapter, SIGNAL(scriptStart(ScriptFiber*)), this, SLOT(addScript(ScriptFiber*)));
    connect(mAdapter, SIGNAL(scriptStop(int)), this, SLOT(removeScript(int)));

    setTitle(tr("Scripting"));
    setOptions(options);
}

void ScriptUi::runScript(const QString &filePath)
{
    mAdapter->run(filePath);
}

void ScriptUi::runSelectedScript()
{
    QModelIndex index = mFileView->currentIndex();

    if (index.isValid() && !mFileModel->isDir(index)) {
        QString scriptFilePath = mFileModel->filePath(index);
        runScript(scriptFilePath);
    }
}

void ScriptUi::abortScript()
{
    if (!mScriptList->currentItem())
        return;

    ScriptFiber *script = mAdapter->script(mScriptList->currentIndex().row());
    if (!script)
        return;

    script->requestTerminate();
    mScriptList->item(mScriptList->currentItem()->row(), 1)->setText(tr("aborting"));
}

void ScriptUi::togglePauseScript()
{
    if (!mScriptList->currentItem())
        return;

    ScriptFiber *script = mAdapter->script(mScriptList->currentIndex().row());
    if (!script)
        return;

    script->togglePause();
    if (script->isPaused())
        mScriptList->item(mScriptList->currentItem()->row(), 1)->setText(tr("paused"));
    else
        mScriptList->item(mScriptList->currentItem()->row(), 1)->setText(tr("running"));
}

void ScriptUi::addScript(ScriptFiber *script)
{
    int newRow = mScriptList->rowCount();
    mScriptList->insertRow(newRow);
    mScriptList->setItem(newRow, 0, new QTableWidgetItem(script->name()));
    mScriptList->setItem(newRow, 1, new QTableWidgetItem(tr("running")));
}

void ScriptUi::removeScript(int index)
{
    mScriptList->removeRow(index);
}

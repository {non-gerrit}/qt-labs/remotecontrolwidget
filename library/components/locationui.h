#ifndef LOCATIONUI_H
#define LOCATIONUI_H

#include "remotecontrolwidget_global.h"
#include "toolbox.h"

#include <QtCore/QDateTime>

class LocationScriptInterface;
class QLineEdit;
class QDateTimeEdit;

class REMOTECONTROLWIDGETSHARED_EXPORT LocationUi : public ToolBoxPage
{
    Q_OBJECT
public:
    struct LocationData
    {
        double latitude;
        double longitude;
        double altitude;
        QDateTime timestamp;
    };

public:
    explicit LocationUi(QWidget *parent = 0);
    virtual ~LocationUi();

    LocationScriptInterface *scriptInterface() const;

public slots:
    void setDisplayedLocation(const LocationData &location);

signals:
    void locationChanged(const LocationData &location) const;

private slots:
    void updateLocationTime();
    void emitLocationChange() const;

private:
    QLineEdit *mLatitudeEdit;
    QLineEdit *mLongitudeEdit;
    QLineEdit *mAltitudeEdit;
    QDateTimeEdit *mTimeEdit;

    friend class LocationScriptInterface;
    LocationScriptInterface *mScriptInterface;
};

class LocationScriptInterface : public QObject
{
    Q_OBJECT
public:
    LocationScriptInterface(LocationUi *ui);
    virtual ~LocationScriptInterface();

    Q_PROPERTY(double latitude READ latitude WRITE setLatitude)
    Q_PROPERTY(double longitude READ longitude WRITE setLongitude)
    Q_PROPERTY(double altitude READ altitude WRITE setAltitude)
    Q_PROPERTY(QDateTime timestamp READ timestamp WRITE setTimestamp)

    double latitude() const;
    void setLatitude(double);

    double longitude() const;
    void setLongitude(double);

    double altitude() const;
    void setAltitude(double);

    QDateTime timestamp() const;
    void setTimestamp(const QDateTime &dt);

private:
    LocationUi *ui;
};

#endif // LOCATIONUI_H

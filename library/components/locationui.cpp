#include "locationui.h"

#include "optionsitem.h"
#include "toolbox.h"

#include <QtGui/QLineEdit>
#include <QtGui/QDoubleValidator>
#include <QtGui/QPushButton>
#include <QtGui/QBoxLayout>
#include <QtGui/QDateTimeEdit>

LocationUi::LocationUi(QWidget *parent)
    : ToolBoxPage(parent)
    , mLatitudeEdit(0)
    , mLongitudeEdit(0)
    , mAltitudeEdit(0)
    , mTimeEdit(0)
    , mScriptInterface(0)
{
    mScriptInterface = new LocationScriptInterface(this);

    QStringList tags;
    QList<OptionsItem *> optionsList;

    mLatitudeEdit = new QLineEdit("52.5056819");
    mLatitudeEdit->setValidator(new QDoubleValidator(-90, 90, 10, this));
    OptionsItem *item = new OptionsItem(tr("Latitude"), mLatitudeEdit);
    //connect(mLatitudeEdit, SIGNAL(editingFinished()), SLOT(triggerLocationInfoChange()));
    item->setTags(tags);
    optionsList << item;

    mLongitudeEdit = new QLineEdit("13.3232027");
    mLatitudeEdit->setValidator(new QDoubleValidator(-180, 180, 10, this));
    item = new OptionsItem(tr("Longitude"), mLongitudeEdit);
    //connect(mLongitudeEdit, SIGNAL(editingFinished()), SLOT(triggerLocationInfoChange()));
    item->setTags(tags);
    optionsList << item;

    mAltitudeEdit = new QLineEdit("13.3232027");
    mAltitudeEdit->setValidator(new QDoubleValidator(-15000, 15000, 10, this));
    item = new OptionsItem(tr("Altitude"), mAltitudeEdit);
    //connect(mAltitudeEdit, SIGNAL(editingFinished()), SLOT(triggerLocationInfoChange()));
    item->setTags(tags);
    optionsList << item;

    mTimeEdit = new QDateTimeEdit();
    QPushButton *locationTimeButton = new QPushButton(tr("Now"));
    QHBoxLayout *hLayout = new QHBoxLayout();
    hLayout->setContentsMargins(0, 0, 0, 0);
    QVBoxLayout *vLayout = new QVBoxLayout();
    vLayout->addWidget(mTimeEdit);
    vLayout->addWidget(locationTimeButton);
    hLayout->addLayout(vLayout);
    QWidget *tmp = new QWidget();
    tmp->setLayout(hLayout);
    //connect(mTimeEdit, SIGNAL(dateTimeChanged(const QDateTime &)), SLOT(triggerLocationInfoChange()));
    connect(locationTimeButton, SIGNAL(clicked()), this, SLOT(updateLocationTime()));
    item = new OptionsItem(tr("Timestamp"), tmp);
    item->setTags(tags);
    optionsList << item;

    setOptions(optionsList);
    setTitle(tr("Location"));
}

LocationUi::~LocationUi()
{
}

LocationScriptInterface *LocationUi::scriptInterface() const
{
    return mScriptInterface;
}

void LocationUi::updateLocationTime()
{
    mTimeEdit->setDateTime(QDateTime::currentDateTime());
}

void LocationUi::setDisplayedLocation(const LocationData &location)
{
    mLatitudeEdit->setText(QString::number(location.latitude, 'f', 8));
    mLongitudeEdit->setText(QString::number(location.longitude, 'f', 8));
    mAltitudeEdit->setText(QString::number(location.altitude, 'f', 8));
    mTimeEdit->setDateTime(location.timestamp);
}

void LocationUi::emitLocationChange() const
{
    LocationData location;
    location.latitude = mScriptInterface->latitude();
    location.longitude = mScriptInterface->longitude();
    location.altitude = mScriptInterface->altitude();
    location.timestamp = mScriptInterface->timestamp();
    emit locationChanged(location);
}

LocationScriptInterface::LocationScriptInterface(LocationUi *ui)
    : QObject(ui)
    , ui(ui)
{
}

LocationScriptInterface::~LocationScriptInterface()
{
}

double LocationScriptInterface::latitude() const
{
    return ui->mLatitudeEdit->text().toDouble();
}

void LocationScriptInterface::setLatitude(double l)
{
    if (latitude() != l) {
        ui->mLatitudeEdit->setText(QString::number(l, 'f', 8));
        ui->emitLocationChange();
    }
}

double LocationScriptInterface::longitude() const
{
    return ui->mLongitudeEdit->text().toDouble();
}

void LocationScriptInterface::setLongitude(double l)
{
    if (longitude() != l) {
        ui->mLongitudeEdit->setText(QString::number(l, 'f', 8));
        ui->emitLocationChange();
    }
}

double LocationScriptInterface::altitude() const
{
    return ui->mAltitudeEdit->text().toDouble();
}

void LocationScriptInterface::setAltitude(double a)
{
    if (altitude() != a) {
        ui->mAltitudeEdit->setText(QString::number(a, 'f', 8));
        ui->emitLocationChange();
    }
}

QDateTime LocationScriptInterface::timestamp() const
{
    return ui->mTimeEdit->dateTime();
}

void LocationScriptInterface::setTimestamp(const QDateTime &dt)
{
    if (timestamp() != dt) {
        ui->mTimeEdit->setDateTime(dt);
        ui->emitLocationChange();
    }
}

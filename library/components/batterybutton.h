#ifndef BATTERYBUTTON_H
#define BATTERYBUTTON_H

#include "remotecontrolwidget_global.h"

#include <QtGui/QToolButton>

class REMOTECONTROLWIDGETSHARED_EXPORT BatteryButton : public QToolButton
{
    Q_OBJECT
public:
    enum BatteryLevel {
        NoBatteryLevel,
        BatteryCritical,
        BatteryVeryLow,
        BatteryLow,
        BatteryNormal
    };
    Q_ENUMS(BatteryLevel)

public:
    explicit BatteryButton(QWidget *parent = 0);
    virtual ~BatteryButton();

public slots:
    void setDisplayedBatteryLevel(BatteryLevel level);

signals:
    void batteryLevelChanged(BatteryLevel level) const;

private slots:
    void emitBatteryLevelChanged();
};

#endif //BATTERYBUTTON_H

#ifndef POWERBUTTON_H
#define POWERBUTTON_H

#include "remotecontrolwidget_global.h"

#include <QtGui/QToolButton>

class REMOTECONTROLWIDGETSHARED_EXPORT PowerButton : public QToolButton
{
    Q_OBJECT
public:
    enum PowerState {
        UnknownPower,
        BatteryPower,
        WallPower,
        WallPowerChargingBattery
    };
    Q_ENUMS(PowerState)

public:
    explicit PowerButton(QWidget *parent = 0);
    virtual ~PowerButton();

public slots:
    void setDisplayedState(PowerState state);

signals:
    void powerStateChanged(PowerState state) const;

private slots:
    void emitPowerStateChanged();
};

#endif //POWERBUTTON_H

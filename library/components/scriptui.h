#ifndef SCRIPTUI_H
#define SCRIPTUI_H

#include "remotecontrolwidget_global.h"
#include "toolbox.h"

#include <QtCore/QObject>

class ScriptAdapter;
class ScriptFiber;
class QTreeView;
class QFileSystemModel;
class QTableWidget;

class REMOTECONTROLWIDGETSHARED_EXPORT ScriptUi : public ToolBoxPage
{
    Q_OBJECT
public:
    ScriptUi(ScriptAdapter *adapter, QWidget *parent = 0);

private slots:
    void runScript(const QString &filePath);
    void runSelectedScript();
    void abortScript();
    void togglePauseScript();

    void addScript(ScriptFiber *script);
    void removeScript(int index);

private:
    ScriptAdapter *mAdapter;

    QTreeView *mFileView;
    QFileSystemModel *mFileModel;
    QTableWidget *mScriptList;
};

#endif // SCRIPTUI_H

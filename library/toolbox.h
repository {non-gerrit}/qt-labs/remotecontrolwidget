#ifndef TOOLBOX_H
#define TOOLBOX_H

#include "remotecontrolwidget_global.h"
#include "optionsitem.h"

#include <QtGui/QFrame>
#include <QtGui/QWidget>
#include <QtCore/QList>

class ToolBoxPage;
class StyledBar;
class OptionsItem;
class QToolButton;
class QFormLayout;
class QStringList;
class QVBoxLayout;
class QHBoxLayout;

class ToolBox : public QFrame
{
    Q_OBJECT
public:

    explicit ToolBox(bool pagesOpenOnInsertion = false, QWidget *parent = 0, Qt::WindowFlags f = 0);
    virtual ~ToolBox();

public:
    bool addPage(ToolBoxPage *page);
    bool insertPage(int index, ToolBoxPage *page);

    void filtersChanged(const QString &filters);

private:
    bool insertPageIntoLayout(int index, ToolBoxPage *page);
    bool removePageFromLayout(ToolBoxPage *page);
    QVBoxLayout *mLayout;
    int mLargestMinimalInputWidth;

    bool mDefaultPagesOpen;
    QList<ToolBoxPage *> mPages;

private slots:
    void processPageMinimalInputWidth(int newWidth);
};

class REMOTECONTROLWIDGETSHARED_EXPORT ToolBoxPage : public QFrame
{
    Q_OBJECT
    friend class ToolBox;
public:
    explicit ToolBoxPage(QWidget *parent = 0);
    virtual ~ToolBoxPage();

    void setOptions(const QList<OptionsItem *> &options);
    void setTitle(const QString &title);

private slots:
    unsigned int filtersChanged(const QStringList &filters);
    void advancedToggled(OptionsItem *item);

private:
    QToolButton *mButton;
    QWidget *mPage;
    QFormLayout *mInnerLayout;
    QHBoxLayout *mHeaderLayout;
    StyledBar *mHeaderBar;
    QList<OptionsItem *> mOptions;
    bool mAdvancedMode;
    QStringList mLastFilters;
    bool mExplicitOpen;
    int mMinimalInputWidth;

    void init();
    void addAdvancedButtonToHeader();
    void addItem(OptionsItem *item);
    void insertItem(int row, OptionsItem *item);

    void showContent(bool show);

    bool filterItem(const QStringList &filters, OptionsItem * option);

    unsigned int findFilteredItems(const QStringList &filters);
    unsigned int findUnfilteredItems();

    void addItemsAndSpacer(QList<OptionsItem *> items);

    void setFixedInputWidth(int newInputWidth);

private slots:
    void toggleOpenExplicitly();
    void setAdvanced(bool advanced);

signals:
    void minimalInputWidthChanged(int newWidth);
};

#endif //TOOLBOX_H

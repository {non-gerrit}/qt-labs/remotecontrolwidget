#ifndef CONFIGURATIONWIDGET_H
#define CONFIGURATIONWIDGET_H

#include "remotecontrolwidget_global.h"

#include <QtGui/QWidget>

class OptionsItem;
class FilterLineEdit;
class ToolBox;
class ToolBoxPage;
class StyledBar;
class ManhattanStyle;
class QToolButton;
class QFrame;
class QLayout;

class REMOTECONTROLWIDGETSHARED_EXPORT RemoteControlWidget: public QWidget
{
    Q_OBJECT
public:
    explicit RemoteControlWidget(QWidget *parent = 0);
    ~RemoteControlWidget();

    virtual void writeSettings(const QString &vendor, const QString &name) const;
    virtual void readSettings(const QString &vendor, const QString &name);

    void addToolBoxPage(ToolBoxPage *page);
    void addMenuButton(QToolButton *button);

protected:
    virtual void initializeViewArea(QFrame *target);

private slots:
    void filtersChanged();
    void toggleViewPaneVisible();

private:
    StyledBar *initializeMenuBar();

private:
    ToolBox *mToolBox;
    FilterLineEdit *mFilterLineEdit;
    QLayout *mMenuLayout;
    QToolButton *mViewButton;
    QFrame *mViewArea;

    ManhattanStyle *mManhattanStyle;
};

#endif //CONFIGURATIONWIDGET_H

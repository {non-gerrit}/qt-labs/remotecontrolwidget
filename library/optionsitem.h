#ifndef OPTIONSITEM_H
#define OPTIONSITEM_H

#include "remotecontrolwidget_global.h"

#include <QtCore/QObject>
#include <QtCore/QStringList>

class QLayout;
class QLabel;
class QHBoxLayout;

class REMOTECONTROLWIDGETSHARED_EXPORT OptionsItem: public QObject
{
    Q_OBJECT
public:
    enum OptionsType {
        DefaultType,
        FullWidthType,
        AdvancedType
    };

    OptionsItem(const QString &name, QWidget *inputWidget, bool fullWidth = false, QObject *parent = 0);
    virtual ~OptionsItem();

    QLabel *label() const;
    QWidget *inputWidget() const;
    QHBoxLayout *inputLayout() const;

    void setVisible(bool visible, bool explicitly = false);
    bool setAdvancedPage(OptionsItem *advancedPage);
    OptionsItem *advancedPage() const;

    bool addTag(const QString &tag);
    bool addTags(const QStringList &tags);
    bool removeTag(const QString &tag);
    bool removeTags(const QStringList &tags);
    bool setTags(const QStringList &tags);
    bool matchTag(const QString &tag) const;
    OptionsType type() const;
    bool isExplicitlyHidden() const;
    void setAdvanced(bool advanced = true);
    bool isAdvanced() const;

private:
    friend class ToolBoxPage;
    QLabel *mLabel;
    QWidget *mInputWidget;
    QHBoxLayout *mInputLayout;
    OptionsItem *mAdvancedPage;
    QStringList mTags;
    OptionsType mType;
    bool mHidden;
    bool mAdvanced;

private slots:
    void buttonClicked();

signals:
    void toggleAdvanced(OptionsItem *item);
};

#endif //OPTIONSITEM_H
